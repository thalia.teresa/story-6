from django import forms
from .models import ModelTDD

class FormTDD(forms.ModelForm):
    class Meta(object):
        model = ModelTDD
        fields = [
            'status',
        ]