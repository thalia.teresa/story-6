from django.shortcuts import render,redirect
from .forms import FormTDD
from .models import ModelTDD
from datetime import datetime
# Create your views here.

def index(request):
    modelTDD = ModelTDD.objects.all()
    context = {
        'hasilStatus' : modelTDD
    }
#    return render(request, 'testimony.html', context)

#def show(request):
    if request.method == 'POST':
        form = FormTDD(request.POST)
        if form.is_valid():
            status = request.POST['status']
            # waktu = request.POST['waktu']
            addStatus = ModelTDD(status=status)
            addStatus.save()
#    return redirect('/testimony/')
    return render(request, 'index.html', context)
