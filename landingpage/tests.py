from django.http import HttpRequest
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from django.utils import timezone
from . import views
import time
import unittest
from .views import *
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class UnitTest(TestCase):
	def test_home_page_status_code(self):
		response = self.client.get('/')
		self.assertEquals(response.status_code, 200)

	def test_view_uses_correct_template(self):
		response = self.client.get(reverse('index'))
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
	
	def test_home_page_contains_correct_html(self):
		response = self.client.get('/')
		self.assertContains(response, 'Halo! Apa Kabar?')
	
	def test_home_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)
	
	def test_home_contains_form(self):
		response = self.client.get('/')
		self.assertIn('status', response.content.decode())
	
	def test_home_create_new_model(self):
		new_status = ModelTDD.objects.create(status="PPW2019", waktu=timezone.now())
		count_status = ModelTDD.objects.all().count()
		self.assertEqual(count_status, 1)

	def test_home_form_is_valid(self):
		form = FormTDD(data={'status':'', 'waktu':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			['This field is required.']
		)

	def test_home_status_is_completed(self):
		test_str = 'Saya bahagia'
		response_post = Client().post('/', {'pesan':test_str, 'waktu':timezone.now})
		self.assertEqual(response_post.status_code, 200)
		response = Client().get('/')
		html_response = response.content.decode('UTF-8')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_op = Options()
        chrome_op.add_argument('--dns-prefetch-disable'),
        chrome_op.add_argument('--no-sandbox'),
        chrome_op.add_argument('--headless'),
        chrome_op.add_argument('disable-gpu'),
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_op)
        super(FunctionalTest, self).setUp()
    
    def test_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        new_status = selenium.find_element_by_id("id_status")
        new_status.send_keys('Coba coba')
        time.sleep(3)
        submit = selenium.find_element_by_id("id_submit")
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        selenium.get(self.live_server_url)
        time.sleep(3)
        self.assertIn('Coba coba', selenium.page_source)
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()
